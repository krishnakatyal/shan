# This is the UI logic for ShAn
# ShAn uses already available annovar tools in making a user interface for in r shiny
# Author: Venkat Subramaniam Rathinakannan
# University of Turku


##
#source('package_loader.R')
library(shiny)
library(shinyjs)
library(shinydashboard)
library(DT)
library(knitr)
library(rmarkdown)
##


#con <- file("shiny.log")
#sink(con, append=TRUE)
#sink(con, append=TRUE, type="message")


dbchoices <<- c('refGene', 'knownGene', 'ensGene')
filterdbs <<- c('1000g2015aug','avsnp150','ljb23_pp2hvar','esp6500siv2_all','exac03','gerp++gt2','cg46','dbnsfp35a','gnomad211_genome') 

shinyUI(dashboardPage(
  dashboardHeader(title = "ShAn",  dropdownMenu(type = "notifications", 
                  icon = icon("question-circle"),
                  badgeStatus = NULL,
                  headerText = "See also:",
                  
                  notificationItem("shiny", icon = icon("file"),
                                   href = "http://shiny.rstudio.com/")
                  )),
  dashboardSidebar(
    h4(selectInput('selectMode', label = 'Select', choices = c('multi','filter','FAQs'), selected = 'filter')),
    tags$hr(),
  
    conditionalPanel(condition = 'input.selectMode == "multi"',
                     selectInput(inputId = 'organism2', label = 'Organism', choices = 
                                   c('human hg19',
                                     'human hg18'), selected = 'human hg19'),
                     checkboxGroupInput('databases2', 'Databases', choices = dbchoices, selected = 'refGene'),
                     fileInput('upload', label = 'Upload file',multiple = TRUE, accept = c('text/avinput', 'text/vcf')),
                     actionButton('submitMulti', 'Submit'),
                     tags$hr(),
                     downloadButton(outputId = 'download2', label = 'Download')
    ),
    conditionalPanel(condition = 'input.selectMode == "filter"',
                     selectInput(inputId = 'organism3', label = 'Organism',  choices =
                                   c('human hg19', 'human hg18'), selected = 'human hg19'),
                     checkboxGroupInput('databases3', 'Databases', choices = filterdbs, selected = 'cg46'),
                     fileInput('uploadFilter', label = 'Upload file', accept = c('text/avinput', 'text/vcf')),
                     actionButton('submitFilter', 'Submit'),
                     downloadButton(outputId = 'download3', label = 'Download')
                     
    )
    
  ),
  dashboardBody(
              
                conditionalPanel(condition = 'input.selectMode == "single"', DT::dataTableOutput('outdata')),
                conditionalPanel(condition = 'input.selectMode == "multi"',DT::dataTableOutput('outdata2')),
                conditionalPanel(condition = 'input.selectMode == "filter"',DT::dataTableOutput('outdata3')),
                conditionalPanel(condition = 'input.selectMode =="FAQ"', uiOutput('mkdown'))
  
  
))
)