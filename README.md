# ShAn: Web based annotation tool

To run the app the system on which it is being executed  should have the latest version of R and RStudio. The following steps illustrate how it could be done :

Step 1 : Install R
1. Install R by visiting the site https://cran.r-project.org/ .
Supplementary Figure 1: screenshot of https://cran.r-project.org/.

2. Run the installer with the default settings. If you don’t have admin rights on your system ask your IT support to provide the required permissions to the R directories. This is important for installing future R packages.

Step 2 : Install RStudio 
1. Download RStudio from https://rstudio.com/products/rstudio/download/#download .

Supplementary Figure 2 : Screenshot of https://rstudio.com/products/rstudio/download/#download

2. After the installation of R has completed successfully ( and not before ) run the RStudio installer.

3. Administrative rights need to be granted for RStudio to be installed successfully. If there are problems regarding this please contact your local IT support for the appropriate rights to be granted.



Step 3 : Check that R and RStudio are working.
1. Open RStudio and the screen should look similar to the figure 3 shown below.
2. In left pane beside the ‘>’ symbol, type ‘4+5’ (without quotes) and hit enter. If the output is ‘9’ that means R and RStudio are working. If this isn’t successful please contact your IT support for further assistance.

Supplementary Figure 3: Startup page of RStudio running with R.

Step 4: Installing the required packages

1. To run the tool ShAn the following packages need to be installed: shiny, shinyjs, shinydashboard, DT, knitr and rmarkdown.

2. This can be done by typing the following command in the left window as seen in figure 3. The commands are :
install.packages("shiny")
install.packages("shinyjs")
install.packages("shinydashboard")
install.packages("DT")
install.packages("knitr")
install.packages("rmarkdown")



Step 5 : Running the tool

1. Now download the entire repository from https://gitlab.utu.fi/vesura/ShAn.git and extract them to your system. 

2. pen the file named “ui.R” and as seen in Supplementary Figure 4.In the right section select three dots button ,A file exploral window will pop up select the ShAn folder ,now blue "More" option select "set as working directory".

3. click on the “run app” button on the top right hand side of the pane.

4. If the “run app” button does not appear please contact your IT support for further assistance.

Supplementary Figure 4: Screenshot after opening the ui.R file.



Step 6: Uploading and annotating a vcf file

The subsequent steps shows how to upload a vcf file and initiate the process of annotation.

1. After the tool has been run from the RStudio interface . The startup page for the app looks has shown in Supplementary Figure 5. The option selected here under the “Select” header is “multi” and the subsequent database choices are listed below the human genome version. Supplementary Figure 6 shows the different options listed below the “Select” header .

Supplementary Figure 5: Screenshot of startup page of ShAn tool.

Supplementary Figure 6: Screenshot of options under the “Select” header.

2. Clicking the “Browse” button displays a window which should be used to select the vcf file of choice and upload for annotation. The file chosen here is “PrCa_sample.vcf” available in the GitLab repository.
Supplementary Figure 6: Screenshot showing the window to upload the file

3. Once the file has been selected and submit button is clicked the upload process starts and a progress bar is displayed at the right hand corner of the screen as shown in Supplementary Figure 7, this shows the progress of the annotation.

Supplementary Figure 7: Screenshot showing the progress bar at the bottom




4. The results for the annotated file are displayed in the left pane as can be seen in Supplementary figure 8. The result file is also available in the GitLab repository under the name “PrCa_Sample_ShAn.txt”


Supplementary Figure 8: Screenshot showing the annotated list on the left pane.


5. The results can also be downloaded by clicking on the download button. This opens a new window where the file can be saved to the users preferred location as seen in Supplementary Figure 9.

Supplementary Figure 9: Screenshot showing download window to select location to save the file in the local system.


## Authors
### Rathinakannan, Venkat Subramaniam<br>
### Schkov, Hannu-Pekka<br>
### Schleutker, Johanna<br>
### Sipeky, Csilla<br>

## License

[MIT](https://choosealicense.com/licenses/mit/)


