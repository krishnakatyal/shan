#### Q1. What are the packages used in the app?   

**A:** The packages requried are DT, shiny, shinyjs, shinydashboard and data.table. These are directly available from CRAN. 

#### Q2. What are the files which can be used as input here?

**A:** The files which can be used as input are vcf files and avi files.

#### Q3. Can the annotated files be downloaded?

**A:** The annotated files can be downloaded using the download button available on the bottom of the collapsible menu present on the left side of the application window.

#### Q4. Does the application have a maximum upload limit for the input files?

**A:** The maximum upload size has been limited to 20 mb for faster and easier annotation of the data. This was done to reduce the computing time and resources needed to annotate the data.



